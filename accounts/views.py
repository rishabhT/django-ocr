from django.shortcuts import render,redirect,HttpResponse
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login,authenticate
from accounts.forms import SignUpForm
from .models import Upload
from django.views.generic.edit import FormView
from .forms import FileFieldForm
import argparse
import cv2
from PIL import Image
import pytesseract
from pdf2image import convert_from_path
import os
import pandas as pd


pixels = []
refPt = []
cropping = False


def append_df_to_excel(df, excel_path):
   df_excel = pd.read_excel(excel_path)
   result = pd.concat([df_excel, df], ignore_index=True)
   result = result.fillna(0)
   result.to_excel(excel_path, index=False)

#This "click_and_crop" function will track mouse click events and will crop the image as well.
def click_and_crop(event, x, y, flags, param):
    global refPt, cropping
    if event == cv2.EVENT_LBUTTONDOWN:
        refPt = [(x, y)]
        cropping = True

    elif event == cv2.EVENT_LBUTTONUP:
        refPt.append((x, y))
        cropping = False
        cv2.rectangle(image, refPt[0], refPt[1], (0, 255, 0), 2)
        cv2.imshow("image", image)

def home(request):
    return render(request,"accounts/home.html")

def signup_view(request):
    form = SignUpForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        form.save()
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password1')
        user     = authenticate(username="username",password="password")
        login(request,user)
        return redirect('/')
    else:
        form = SignUpForm()
    return render(request,"accounts/signup.html",{'form':form})


# def upload(request):
#     files = request.FILES.getlist("files")
#     if files:
#         for file in files:
#             instance =  Upload(fileupload=file)
#             instance.save()
#     else:
#         print("error")  
#     return render(request,"accounts/upload.html")      

def upload(request):
    files = request.FILES.getlist("files")
    df1 = pd.DataFrame()
    if files:
        for file in files:
            instance =  Upload(fileupload=file)
            instance.save()
            fields = []
            print(file)
            pages = convert_from_path(r"/home/rishabh/pdf_ocr/media/"+str(file))
            for page in pages:
                page.save('out.png', 'PNG')

            if pixels:
                for i in pixels:
                    image = cv2.imread("out.png")
                    clone = image.copy()
                    if len(i) == 2:
                        roi = clone[i[0][1]:i[1][1], i[0][0]:i[1][0]]     
                        cv2.imwrite("test.png",roi)
                        cv2.waitKey(0)
                    fields.append(pytesseract.image_to_string(Image.open('test.png')))
                    cv2.destroyAllWindows()

            else:        
                for i in range(5):       
                    image = cv2.imread("out.png")
                    clone = image.copy()
                    cv2.namedWindow("image")
                    cv2.setMouseCallback("image", click_and_crop)

                    while True:
                        cv2.imshow("image", image)
                        key = cv2.waitKey(1) & 0xFF
                        if key == ord("r"):
                            image = clone.copy()
                        elif key == ord("c"):
                            break
        #This will save image pixels co-ordinates so that we can further use them in automation.
                    if len(refPt) == 2:
                        roi = clone[refPt[0][1]:refPt[1][1], refPt[0][0]:refPt[1][0]]     
                        cv2.imwrite("test.png",roi)
                        cv2.waitKey(0)
                    fields.append(pytesseract.image_to_string(Image.open('test.png')))
                    pixels.append(refPt)
                    cv2.destroyAllWindows()
            df = pd.DataFrame([fields])
            df1 = df1.append(df,ignore_index=True)
            append_df_to_excel(df,"/home/rishabh/pdf_ocr/pdfdata.xlsx")
 

    else:
        print("error")  
    return render(request,"accounts/upload.html")
def index2(request):
    df = pd.read_excel("/home/rishabh/pdf_ocr/pdfdata.xlsx")

    return render(request,"accounts/table.html",{'df':df })